# Sitemap Generator

Branch für Shopware 5.2

Genaue Plugin Infos in plugin.xml

**eigene Anpassungen im Plugin:**

- <changefreq> und  <priority> tags gelöscht in /Resources/views/frontend/pixup_sitemap/entry.tpl

von daher ist folgender Workaround bei Plugin Updates:

- Update im Backend durchführen
- Anpassungen übernehmen
- Ordner /plugins/PixupSitemapGenerator via ftp downloaden
- in das Repo unter dem Plugin Release Tag pushen 
- somit ist sichergestellt, dass der aktuelle Code + Anpassung im Repo ist