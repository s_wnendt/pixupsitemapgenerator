{if $urlParams}
    <url>
        {if $seoUrl}
            <loc>{$urlParams|escape}</loc>
        {else}
            {assign var="seourl" value={url params = $urlParams}}
            <loc>{$seourl|escape}</loc>
            {foreach from=$shops item=shop}
            <xhtml:link rel="alternate" hreflang="{$shop.locale|replace:'_':'-'|lower}" href="{shopseo params=$urlParams shopId=$shop.id|escape}"/>
            {/foreach}
        {/if}
        {if $lastmod}
            <lastmod>{date_format($lastmod, 'Y-m-d')}</lastmod>
        {/if}
    </url>
{/if}
