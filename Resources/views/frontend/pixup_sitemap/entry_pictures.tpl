{if $entry.urlParams}
    <url>
        {if $seoUrl}
            <loc>{$entry.urlParams|escape}</loc>
        {else}
            {assign var="seourl" value={url params = $entry.urlParams}}
            <loc>{$seourl|escape}</loc>
            {foreach from=$shops item=shop}
                <xhtml:link rel="alternate" hreflang="{$shop.locale|replace:'_':'-'}" href="{shopseo params=$entry.urlParams shopId=$shop.id|escape}"/>
            {/foreach}
        {/if}
        {strip}
        {foreach $entry.images as $image}
            <image:image>
                <image:loc>{$image}</image:loc>
                {*<image:title>{$image.name}</image:title>*}
            </image:image>
        {/foreach}
        {/strip}
    </url>
{/if}


