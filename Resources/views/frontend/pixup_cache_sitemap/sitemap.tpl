<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"{if $area == "pictures"} xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"{/if} {if $config.useAlternate} xmlns:xhtml="http://www.w3.org/1999/xhtml"{/if}>
    {strip}
        {if $area == "pictures"}
            {foreach $data as $entry}
                {include file="frontend/pixup_cache_sitemap/entry_pictures.tpl" shops=$languageShops shopId=$shopId entry=$entry}
            {/foreach}
        {elseif $area == "landingPages"}
            {foreach $data as $entry}
                {include file="frontend/pixup_cache_sitemap/entry.tpl" shops=$languageShops shopId=$shopId urlParams=$entry.urlParams lastmod=$entry.changed changefreq=$entry.pixupProperties.changefreq priority=$entry.pixupProperties.priority seoUrl=$entry.pixupProperties.is_seo_landing}
            {/foreach}
        {else}
            {foreach $data as $entry}
                {include file="frontend/pixup_cache_sitemap/entry.tpl" shops=$languageShops shopId=$shopId urlParams=$entry.urlParams lastmod=$entry.changed changefreq=$entry.pixupProperties.changefreq priority=$entry.pixupProperties.priority}
            {/foreach}
        {/if}
    {/strip}
</urlset>
