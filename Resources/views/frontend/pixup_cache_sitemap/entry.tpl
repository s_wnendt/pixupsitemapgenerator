{if $urlParams}
    <url>
        <loc>{shopseo params=$urlParams shopId=$shopId}</loc>
        {foreach from=$shops item=shop}
        <xhtml:link rel="alternate" hreflang="{$shop.locale|replace:'_':'-'|lower}" href="{shopseo params=$urlParams shopId=$shop.id}"/>
        {/foreach}
        {if $lastmod}
            <lastmod>{date_format($lastmod, 'Y-m-d')}</lastmod>
        {/if}
        <changefreq>{if $changefreq and $changefreq|trim != ''}{$changefreq}{else}weekly{/if}</changefreq>
        <priority>{if $priority and $priority|trim != ''}{$priority|string_format:"%.1f"}{elseif $priority == '0.0'}0.0{else}0.5{/if}</priority>
    </url>
{/if}
