{if $entry.urlParams}
    <url>
        <loc>{shopseo params=$entry.urlParams shopId=$shopId}</loc>
        {foreach from=$shops item=shop}
            <xhtml:link rel="alternate" hreflang="{$shop.locale|replace:'_':'-'}" href="{shopseo params=$entry.urlParams shopId=$shop.id}"/>
        {/foreach}
        {strip}
        {foreach $entry.images as $image}
            <image:image>
                <image:loc>{$image}</image:loc>
                {*<image:title>{$image.name}</image:title>*}
            </image:image>
        {/foreach}
        {/strip}
    </url>
{/if}


