<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    {strip}
        {foreach $sitemaps as $sitemap}
            <sitemap>
                <loc>{shopseo params=$sitemap.url shopId=$shopId}</loc>
                {if $lastmod}
                    <lastmod>{date_format($lastmod, 'Y-m-d')}</lastmod>
                {/if}
            </sitemap>
        {/foreach}
    {/strip}
</sitemapindex>
