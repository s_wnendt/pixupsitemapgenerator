<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    {strip}
    {foreach $sitemaps as $sitemap}
        {assign var="area" value=$sitemap.name|cat: "-sitemap.xml"}
        <sitemap>
            {assign var="seourl" value={url module="frontend" controller="PixupSitemap" action="sitemap" area=$area}}
            <loc>{$seourl|escape}</loc>
            {if $lastmod}
                <lastmod>{date_format($lastmod, 'Y-m-d')}</lastmod>
            {/if}
        </sitemap>
    {/foreach}
    {/strip}
</sitemapindex>
