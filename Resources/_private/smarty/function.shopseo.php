<?php
function smarty_function_shopseo($params, Enlight_Template_Default $template)
{
    $modelManager = Shopware()->Models();
    $shop = $modelManager->getRepository(\Shopware\Models\Shop\Shop::class)->getById($params['shopId']);
    if(empty($shop)) return '';

    $shopContext = \Shopware\Components\Routing\Context::createFromShop($shop, Shopware()->Container()->get('config'));
    $query = (array) $params['params'];
    return Shopware()->Front()->Router()->assemble($query, $shopContext);
}
