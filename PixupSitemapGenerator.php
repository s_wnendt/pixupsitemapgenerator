<?php

namespace PixupSitemapGenerator;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\Tools\SchemaTool;
use PixupSitemapGenerator\Components\CompilerPass\AddTemplatePluginDirCompilerPass;
use PixupSitemapGenerator\Helper\SitemapGenerator;
use PixupSitemapGenerator\Models\PixupSitemapCache;
use PixupSitemapGenerator\Models\ShopDecorator;
use Shopware\Bundle\AttributeBundle\Service\CrudService;
use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;
use Shopware\Components\Plugin\Context\UpdateContext;
use Shopware\Models\Shop\Shop;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class PixupSitemapGenerator extends Plugin {
    const SITEMAP_FILE_CACHE_DIRE = __DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR;
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new AddTemplatePluginDirCompilerPass());
    }

    public static function getSubscribedEvents(){
        return [
            'Enlight_Controller_Action_PostDispatch_Frontend_SitemapXml' => 'onPostDispatchSitemapXml',
            'Enlight_Controller_Action_PostDispatch_Frontend_RobotsTxt' => 'onPostDispatchSitemapXml',
            'Shopware_Controllers_Frontend_SitemapXml::indexAction::replace' => 'replaceSitemapXmlIndex',
            'Enlight_Controller_Action_PostDispatchSecure_Frontend_Sitemap' => 'onActionPostDispatchSecureFrontendSitemap',
            'Enlight_Controller_Dispatcher_ControllerPath_Frontend_PixupSitemap' => 'registerController',
            'Shopware_CronJob_PixupSitemapCron' => 'PixupSitemapCronRun'
        ];
    }

    /*     * *************************************************************************************************************** */

    /**
     * Add variables to sitemap
     * @param \Enlight_Controller_ActionEventArgs $args
     */
    public function onActionPostDispatchSecureFrontendSitemap(\Enlight_Controller_ActionEventArgs $args) {
        $template = $this->container->get('template');
        $template->addTemplateDir(
                $this->getPath() . '/Resources/views/'
        );

        $config = $this->container->get('config');

        $template->assign(array(
            'useSuppliers' => $config->getByNamespace('PixupSitemapGenerator', 'useSuppliers'),
            'useLandingPages' => $config->getByNamespace('PixupSitemapGenerator', 'useLandingPages'),
            'useCustomPages' => $config->getByNamespace('PixupSitemapGenerator', 'useCustomPages'),
        ));
    }

    /**
     * @param \Enlight_Controller_ActionEventArgs $args
     */
    public function onPostDispatchSitemapXml(\Enlight_Controller_ActionEventArgs $args) {
        $this->container->get('template')->addTemplateDir(
                $this->getPath() . '/Resources/views/'
        );
    }

    /**
     * @param \Enlight_Hook_HookArgs $arguments
     */
    public function replaceSitemapXmlIndex(\Enlight_Hook_HookArgs $arguments) {
        $config = $this->container->get('config');
        $limit = $config->getByNamespace('PixupSitemapGenerator', 'urlLimit');
        $useArticles = $config->getByNamespace('PixupSitemapGenerator', 'useArticles');
        $useCategories = $config->getByNamespace('PixupSitemapGenerator', 'useCategories');
        $useBlogs = $config->getByNamespace('PixupSitemapGenerator', 'useBlogs');
        $useCustomPages = $config->getByNamespace('PixupSitemapGenerator', 'useCustomPages');
        $useSuppliers = $config->getByNamespace('PixupSitemapGenerator', 'useSuppliers');
        $useLandingPages = $config->getByNamespace('PixupSitemapGenerator', 'useLandingPages');
        $usePictures = $config->getByNamespace('PixupSitemapGenerator', 'usePictures');
        $sitemaps = array();

        $cache = $config->getByNamespace('PixupSitemapGenerator', 'useCache');
        if ($cache == '1') {
            $arguments->getSubject()->Response()->setHeader('Content-Type', 'text/xml; charset=utf-8');
            set_time_limit(0);
            if ($useCategories) {
                // determine count of categories
                $count = $this->getSitemapCountCache('categories', $limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'categories-' . $i);
                }
            }

            if ($useArticles) {
                // determine count of articles
                $count = $this->getSitemapCountCache('articles', $limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'articles-' . $i);
                }
            }

            if ($useCustomPages) {
                // determine count of customPages
                $count = $this->getSitemapCountCache('customPages', $limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'customPages-' . $i);
                }
            }

            if ($useSuppliers) {
                // determine count of suppliers
                $count = $this->getSitemapCountCache('suppliers', $limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'suppliers-' . $i);
                }
            }

            if ($useLandingPages) {
                // determine count of landingPages
                $count = $this->getSitemapCountCache('landingPages', $limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'landingPages-' . $i);
                }
            }

            if ($useBlogs) {
                // determine count of blogs
                $count = $this->getSitemapCountCache('blogs', $limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'blogs-' . $i);
                }
            }

            if ($usePictures) {
                // determine count of pictures
                $count = $this->getPicturesCountCache($limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'pictures-' . $i);
                }
            }
            $arguments->getSubject()->View()->sitemaps = $sitemaps;
        } elseif ($cache == '2') {
            $shopId = Shopware()->Shop()->getId();
            $filename = \PixupSitemapGenerator\PixupSitemapGenerator::SITEMAP_FILE_CACHE_DIRE . 'index' . '_' . $shopId . '.xml';
            header('Content-Type: text/xml');
            if(file_exists($filename)) {
                $content = file_get_contents($filename);
            } else {
                $content = "Please Execute the Cronjob in order to generate the Pixupmedia-Sitemap";
            }
            $arguments->getSubject()->View()->setTemplate(null);
            $arguments->getSubject()->Response()->setHeader('Content-Type', 'text/xml');
            return $arguments->getSubject()->Response()->setBody($content);
        } else {
            $arguments->getSubject()->Response()->setHeader('Content-Type', 'text/xml; charset=utf-8');
            set_time_limit(0);
            /** @var \Shopware\Components\SitemapXMLRepository $sitemap */
            $sitemap = $this->container->get('sitemapxml.repository');
            $data = $sitemap->getSitemapContent();

            $sitemaps = array();

            if ($useCategories) {
                // determine count of categories
                $count = $this->getSitemapCount($data['categories'], $limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'categories-' . $i);
                }
            }

            if ($useArticles) {
                // determine count of articles
                $count = $this->getSitemapCount($data['articles'], $limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'articles-' . $i);
                }
            }

            if ($useCustomPages) {
                // determine count of customPages
                $count = $this->getSitemapCount($data['customPages'], $limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'customPages-' . $i);
                }
            }

            if ($useSuppliers) {
                // determine count of suppliers
                $count = $this->getSitemapCount($data['suppliers'], $limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'suppliers-' . $i);
                }
            }

            if ($useLandingPages) {
                // determine count of landingPages
                $count = $this->getLandingPageCount($data['landingPages'], $limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'landingPages-' . $i);
                }
            }

            if ($useBlogs) {
                // determine count of blogs
                $count = $this->getSitemapCount($data['blogs'], $limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'blogs-' . $i);
                }
            }
            if ($usePictures) {
                // determine count of pictures
                $count = $this->getPicturesCount($limit);
                for ($i = 0; $i < $count; $i++) {
                    $sitemaps[] = array('name' => 'pictures-' . $i);
                }
            }
            $arguments->getSubject()->View()->sitemaps = $sitemaps;
        }
    }

    /* ------------------------- Helper functions ------------------------- */

    private function addSnippets() {
        $snippets = $this->container->get('snippets');
        $snippets->addConfigDir($this->getPath() . 'Resources/snippets/');
    }

    private function getLandingPageCount($data, $limit) {
        $length = count($data);
        $existQuery = "SHOW TABLES LIKE 'tonur_seo_filter_landingpages'";
        $tableExists = Shopware()->Db()->fetchAll($existQuery);
        if (count($tableExists) > 0) {
            $context = Shopware()->Container()->get('shopware_storefront.context_service')->getShopContext();
            $shopId = $context->getShop()->getId();
            $sqlLandingPages = "SELECT * FROM tonur_seo_filter_landingpages tl WHERE tl.enabled=1 AND tl.shop_id=" . $shopId . ";";
            $lp = Shopware()->Db()->fetchAll($sqlLandingPages);
            $lengthSeo = count($lp);
            $length = $length + $lengthSeo;
        }
        if ($length == 0) {
            return 0;
        }
        if ($length > $limit) {
            $count = $length / $limit;
        } else {
            $count = 1;
        }

        return $count;
    }

    /**
     * count how many picutres there are
     * @param $limit
     * @return float|int
     */
    private function getPicturesCount($limit) {

        $data = $this->getPicturesSitemap();
        $data_array = array();
        $index = 0;
        $maxImageCountPerLoc = 0;

        foreach ($data as $loc) {
            $maxImageCountPerLoc += count($loc['images']);
            if ($maxImageCountPerLoc < $limit) {
                $data_array[$index][] = $loc;
            } else {
                $index++;
                $maxImageCountPerLoc = 0;
                $data_array[$index][] = $loc;
                $maxImageCountPerLoc += count($loc['images']);
            }
        }

        return count($data_array);
    }

    /**
     * count how many picutres there are
     * @param $limit
     * @return float|int
     */
    private function getPicturesCountCache($limit) {
        $context = Shopware()->Container()->get('shopware_storefront.context_service')->getShopContext();
        $shopId = $context->getShop()->getId();
        $sql = "SELECT * FROM pixup_sitemap_cache WHERE type = ? AND shop_id = ?";
        $data = Shopware()->Db()->fetchAll($sql, array('pictures', $shopId));
        $data_array = array();
        $index = 0;
        $maxImageCountPerLoc = 0;

        foreach ($data as $loc) {
            $loc['images'] = json_decode($loc['links']);
            $maxImageCountPerLoc += count($loc['images']);
            if ($maxImageCountPerLoc < $limit) {
                $data_array[$index][] = $loc;
            } else {
                $index++;
                $maxImageCountPerLoc = 0;
                $data_array[$index][] = $loc;
                $maxImageCountPerLoc += count($loc['images']);
            }
        }

        return count($data_array);
    }

    /**
     * Build a sitemap for pictures
     *
     * @return array
     */
    private function getPicturesSitemap() {
        $result = array();
        $mediaService = $this->container->get('shopware_media.media_service');
        $config = $this->container->get('config');
        if ($config->getByNamespace('PixupSitemapGenerator', 'dataBasedOn')==1) {
            $basedOn = 'articleID';
        } else {
            $basedOn = 'id';
        }

         // first, get all articles
        $sqlArticles = "SELECT * FROM s_articles sa JOIN s_articles_attributes saa ON sa.main_detail_id=saa.articledetailsID WHERE sa.active = 1 AND (saa.pixup_exclude_sitemap != 1 OR saa.pixup_exclude_sitemap IS NULL)";
        $articles = Shopware()->Db()->fetchAll($sqlArticles);
        $context = Shopware()->Container()->get('shopware_storefront.context_service')->getShopContext();
        $shopCategory = $context->getShop()->getCategory()->getId();

        foreach ($articles as $article) {
            $sqlCat = "SELECT * FROM s_articles_categories_ro sacr WHERE sacr.articleID = ? AND sacr.categoryID=?";
            $categories = Shopware()->Db()->fetchRow($sqlCat, array($article[$basedOn], $shopCategory));

            if ($categories) {

                $sqlMedia = "SELECT ai.media_id FROM s_articles_img ai WHERE ai.articleID = ?";

                $mediaIDs = Shopware()->Db()->fetchAll($sqlMedia, array($article[$basedOn]));

                if (count($mediaIDs) == 0) {
                    continue;
                } else {
                    // 3rd, get path for detail article
                    $detailPath = "sViewport=detail&sArticle=" . $article[$basedOn];

                    $sqlArticlePath = "SELECT path FROM s_core_rewrite_urls WHERE org_path = " . "\"" . $detailPath . "\" AND main = 1";
                    //$sqlArticlePath = "SELECT path FROM s_core_rewrite_urls WHERE org_path = $detailPath  AND main = 1";
                    $rewritePath = Shopware()->Db()->fetchRow($sqlArticlePath);
                    //$rewritePath['path'] =$mediaService->getUrl($rewritePath['path']);

                    if (is_null($rewritePath['path'])) {
                        $result[$article[$basedOn]]['path'] = "detail/index/sArticle/" . $article[$basedOn];
                    } else {
                        $result[$article[$basedOn]]['path'] = strtolower($rewritePath['path']);
                    }
                    foreach ($mediaIDs as $id) {
                        $sqlImages = "SELECT id, name, path FROM s_media WHERE type = 'IMAGE' AND id = ?";
                        $image = Shopware()->Db()->fetchRow($sqlImages, array($id['media_id']));
                        $url = $mediaService->getUrl($image['path']);
                        $result[$article[$basedOn]]['images'][] = $url;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $data
     * @param $limit
     * @return float|int
     */
    private function getSitemapCountCache($type, $limit) {
        $context = Shopware()->Container()->get('shopware_storefront.context_service')->getShopContext();
        $shopId = $context->getShop()->getId();
        $sql = "SELECT * FROM pixup_sitemap_cache WHERE type = ? AND shop_id = ?";
        $data = Shopware()->Db()->fetchAll($sql, array($type, $shopId));
        $length = count($data);
        if ($length == 0) {
            return 0;
        }
        if ($length > $limit) {
            $count = $length / $limit;
        } else {
            $count = 1;
        }

        return $count;
    }

    /**
     * @param $data
     * @param $limit
     * @return float|int
     */
    private function getSitemapCount($data, $limit) {
        $length = count($data);
        if ($length == 0) {
            return 0;
        }
        if ($length > $limit) {
            $count = $length / $limit;
        } else {
            $count = 1;
        }

        return $count;
    }

    /**
     * Get the array store for the priority comboBox
     *
     * @return array
     */
    private function getPriorityStore() {
        return [
            ['key' => '0.0', 'value' => '0.0'],
            ['key' => '0.1', 'value' => '0.1'],
            ['key' => '0.2', 'value' => '0.2'],
            ['key' => '0.3', 'value' => '0.3'],
            ['key' => '0.4', 'value' => '0.4'],
            ['key' => '0.5', 'value' => '0.5'],
            ['key' => '0.6', 'value' => '0.6'],
            ['key' => '0.7', 'value' => '0.7'],
            ['key' => '0.8', 'value' => '0.8'],
            ['key' => '0.9', 'value' => '0.9'],
            ['key' => '1.0', 'value' => '1.0']
        ];
    }

    /**
     * Get Change Frequency store
     *
     * @return array
     */
    private function getChangeFrequencyStore() {
        $snippets = $this->container->get('snippets');

        return [
            ['key' => 'always', 'value' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/always')],
            ['key' => 'hourly', 'value' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/hourly')],
            ['key' => 'daily', 'value' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/daily')],
            ['key' => 'weekly', 'value' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/weekly')],
            ['key' => 'monthly', 'value' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/monthly')],
            ['key' => 'yearly', 'value' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/yearly')],
            ['key' => 'never', 'value' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/never')]
        ];
    }

    /**
     * Create the custom fieldSet for the sitemap generator options
     *
     * @param CrudService $crudService
     * @param \Shopware_Components_Snippet_Manager $snippets
     * @param $tableName
     */
    private function buildPixupSitemapGeneratorFieldSet($crudService, $snippets, $tableName) {
        $crudService->update($tableName, 'pixup_exclude_sitemap', 'boolean', [
            'displayInBackend' => true,
            'label' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/pixupExcludeFromSitemap'),
                ], null, true, 0);

        $crudService->update($tableName, 'pixup_link_priority', 'combobox', [
            'displayInBackend' => true,
            'helpText' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/pixupLinkPriorityHelp'),
            'label' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/pixupLinkPriority'),
            'arrayStore' => $this->getPriorityStore(),
                ], null, false, '0.5');

        $crudService->update($tableName, 'pixup_link_link_change_frequenzy', 'combobox', [
            'displayInBackend' => true,
            'label' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/pixupLinkChangeFrequency'),
            'helpText' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/pixupLinkChangeFrequencyHelp'),
            'arrayStore' => $this->getChangeFrequencyStore()
                ], null, false, 'weekly');
    }

    /**
     * Delete the custom fieldSets
     * @param CrudService $crudService
     * @param $tableName
     */
    private function deleteCustomFields($crudService, $tableName) {
        try {
            $crudService->delete($tableName, 'pixup_exclude_sitemap');
        } catch (\Exception $e) {
            Shopware()->PluginLogger()->error($e->getMessage());
        }

        try {
            $crudService->delete($tableName, 'pixup_link_priority');
        } catch (\Exception $e) {
            Shopware()->PluginLogger()->error($e->getMessage());
        }

        try {
            $crudService->delete($tableName, 'pixup_link_link_change_frequenzy');
        } catch (\Exception $e) {
            Shopware()->PluginLogger()->error($e->getMessage());
        }
    }

    /* ------------------------- Configuration functions ------------------------- */

    /**
     * Update Attributes Tables to include 'exclude from sitemap'
     */
    private function createDatabase() {
        $crudService = $this->container->get('shopware_attribute.crud_service');
        $snippets = $this->container->get('snippets');

        $crudService->update('s_categories_attributes', 'pixup_exclude_all_categories', 'boolean', [
            'displayInBackend' => true,
            'label' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/pixupExcludeAllCategories'),
        ]);

        $this->buildPixupSitemapGeneratorFieldSet($crudService, $snippets, 's_categories_attributes');
        $this->buildPixupSitemapGeneratorFieldSet($crudService, $snippets, 's_articles_attributes');
        $this->buildPixupSitemapGeneratorFieldSet($crudService, $snippets, 's_articles_supplier_attributes');
        $this->buildPixupSitemapGeneratorFieldSet($crudService, $snippets, 's_blog_attributes');
        $this->buildPixupSitemapGeneratorFieldSet($crudService, $snippets, 's_emotion_attributes');
        $this->buildPixupSitemapGeneratorFieldSet($crudService, $snippets, 's_cms_support_attributes');
        $this->buildPixupSitemapGeneratorFieldSet($crudService, $snippets, 's_cms_static_attributes');

        $this->container->get('models')->generateAttributeModels(['s_articles_attributes', 's_categories_attributes', 's_articles_supplier_attributes',
            's_blog_attributes', 's_emotion_attributes', 's_cms_support_attributes', 's_cms_static_attributes']);
    }

    /**
     * Remove plugin tables from database
     */
    private function removeDatabase() {
        $crudService = $this->container->get('shopware_attribute.crud_service');

        $this->deleteCustomFields($crudService, 's_articles_attributes');
        $this->deleteCustomFields($crudService, 's_articles_supplier_attributes');
        $this->deleteCustomFields($crudService, 's_blog_attributes');
        $this->deleteCustomFields($crudService, 's_categories_attributes');
        $this->deleteCustomFields($crudService, 's_emotion_attributes');
        $this->deleteCustomFields($crudService, 's_cms_support_attributes');
        $this->deleteCustomFields($crudService, 's_cms_static_attributes');

        try {
            $crudService->delete('s_categories_attributes', 'pixup_exclude_all_categories');
        } catch (\Exception $e) {
            Shopware()->PluginLogger()->error($e->getMessage());
        }

        $this->container->get('models')->generateAttributeModels([
            's_articles_attributes',
            's_categories_attributes',
            's_articles_supplier_attributes',
            's_blog_attributes',
            's_emotion_attributes',
            's_cms_support_attributes',
            's_cms_static_attributes'
        ]);

        $cacheDriver = new \Doctrine\Common\Cache\ArrayCache();
        $deleted = $cacheDriver->deleteAll();
    }

    /**
     * @param \Enlight_Event_EventArgs $args
     * @return string
     */
    public function registerController(\Enlight_Event_EventArgs $args) {
        $this->container->get('template')->addTemplateDir(
                $this->getPath() . '/Resources/views/'
        );

        return $this->getPath() . "/Controllers/Frontend/PixupSitemap.php";
    }

    /*     * ************************************* Default Plugin Functions *********************************************** */

    /**
     * @param InstallContext $context
     */
    public function install(InstallContext $context) {
        $this->createDatabase();
        $this->createSchema();
        $this->addCron();
        $snippets = $this->container->get('snippets');
        $snippets->addConfigDir(
                $this->getPath() . 'Resources/snippets/'
        );

        $context->scheduleClearCache(InstallContext::CACHE_LIST_DEFAULT);
    }

    public function uninstall(UninstallContext $context) {
        $this->removeDatabase();
        $this->dropSchema();
        $this->removeCron();
        return true;
    }

    /**
     * Update the plugin
     *
     * @param UpdateContext $context
     * @return bool
     */
    public function update(UpdateContext $context) {
        if (version_compare($context->getCurrentVersion(), '2.0.2', '<')) {
            $crudService = $this->container->get('shopware_attribute.crud_service');
            $snippets = $this->container->get('snippets');

            $this->updatePixupSitemapGeneratorFieldSetFor201($crudService, $snippets, 's_categories_attributes');
            $this->updatePixupSitemapGeneratorFieldSetFor201($crudService, $snippets, 's_articles_attributes');
            $this->updatePixupSitemapGeneratorFieldSetFor201($crudService, $snippets, 's_articles_supplier_attributes');
            $this->updatePixupSitemapGeneratorFieldSetFor201($crudService, $snippets, 's_blog_attributes');
            $this->updatePixupSitemapGeneratorFieldSetFor201($crudService, $snippets, 's_emotion_attributes');
            $this->updatePixupSitemapGeneratorFieldSetFor201($crudService, $snippets, 's_cms_support_attributes');
            $this->updatePixupSitemapGeneratorFieldSetFor201($crudService, $snippets, 's_cms_static_attributes');
        }
        $this->container->get('models')->generateAttributeModels(['s_articles_attributes']);
        $this->container->get('models')->generateAttributeModels(['s_categories_attributes']);
        $this->container->get('models')->generateAttributeModels(['s_articles_supplier_attributes']);
        $this->container->get('models')->generateAttributeModels(['s_blog_attributes']);
        $this->container->get('models')->generateAttributeModels(['s_emotion_attributes']);
        $this->container->get('models')->generateAttributeModels(['s_cms_support_attributes']);
        $this->container->get('models')->generateAttributeModels(['s_cms_static_attributes']);
        if (version_compare($context->getCurrentVersion(), '2.2.1', '<')) {
            $this->createSchema();
            $this->addCron();
        }
        if (version_compare($context->getCurrentVersion(), '2.2.4', '<')) {
            $this->removeOldCron();
            $this->addCron();
        }
        /* if (version_compare($context->getCurrentVersion(), '2.1.5', '<'))
          {
          $sql = 'UPDATE s_articles_attributes SET pixup_exclude_sitemap = 0 WHERE pixup_exclude_sitemap IS NULL';
          $execute = Shopware()->Db()->update('s_articles_attributes',$sql, 'pixup_exclude_sitemap IS NULL');

          } */


        return true;
    }

    /*     * ************************************* Custom Update Methods ************************************************** */

    /**
     * Update the sitemap generator fieldSet
     *
     * @param CrudService $crudService
     * @param \Shopware_Components_Snippet_Manager $snippets
     * @param $tableName
     */
    private function updatePixupSitemapGeneratorFieldSetFor201($crudService, $snippets, $tableName) {
        $crudService->update($tableName, 'pixup_link_priority', 'combobox', [
            'displayInBackend' => true,
            'helpText' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/pixupLinkPriorityHelp'),
            'label' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/pixupLinkPriority'),
            'arrayStore' => $this->getPriorityStore(),
            'defaultValue' => '0.5'
        ]);

        $crudService->update($tableName, 'pixup_link_link_change_frequenzy', 'combobox', [
            'displayInBackend' => true,
            'label' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/pixupLinkChangeFrequency'),
            'helpText' => $snippets->getNamespace('backend/pixup_sitemap_generator/main')->get('detail/base/pixupLinkChangeFrequencyHelp'),
            'arrayStore' => $this->getChangeFrequencyStore(),
            'defaultValue' => 'weekly'
        ]);
    }

    /*     * ************************************* CronJob part ************************************************** */

    public function addCron() {
        $this->container->get('dbal_connection')->executeQuery('DELETE FROM s_crontab WHERE `name` = ?', [
            'PixupSitemapCron'
        ]);
        $now = new \DateTime('now');
        $nowformat = $now->format('Y-m-d');
        $connection = $this->container->get('dbal_connection');
        $connection->insert(
                's_crontab', [
            'name' => 'PixupSitemapCron',
            'action' => 'PixupSitemapCron',
            'next' => new \DateTime($nowformat . ' 01:00:00'),
            'start' => null,
            '`interval`' => '86400',
            'active' => 1,
            'end' => new \DateTime(),
            'pluginID' => null
                ], [
            'next' => 'datetime',
            'end' => 'datetime',
                ]
        );
    }

    public function removeCron() {
        $this->container->get('dbal_connection')->executeQuery('DELETE FROM s_crontab WHERE `name` = ?', [
            'PixupSitemapCron'
        ]);
    }

    public function removeOldCron() {
        $this->container->get('dbal_connection')->executeQuery('DELETE FROM s_crontab WHERE `name` = ?', [
            'MySitemapCron'
        ]);
    }

    public function PixupSitemapCronRun(\Shopware_Components_Cron_CronJob $job) {
        $config = $this->container->get('config');
        switch($config->getByNamespace('PixupSitemapGenerator', 'useCache')){
            case '0': // deactivate
                return false;
                break;
            case '1': // db cache
                $this->generateCache();
                return true;
                break;
            case '2': // file based cache
                $this->generateFileCache();
                return true;
                break;
        }
    }

    public function generateFileCache(){
        $sql = "SELECT * FROM s_core_shops";
        $limit = Shopware()->Config()->getByNamespace('PixupSitemapGenerator', 'urlLimit');
        $shops = Shopware()->Db()->fetchAll($sql);
        foreach ($shops as $shop) {
            $sitemaps = [];
            // foreach area (product,landingpage,picture,suppliers,...)
            foreach ($this->attributeTableMapping as $key => $area) {
                if(!$this->areaGeneratorRule($key)) continue;
                $data = $this->getSitemapForArea($key, $shop);
                set_time_limit(0);
                $data_array = array();
                if ($key == "pictures") {
                    $index = 0;
                    $maxImageCountPerLoc = 0;
                    foreach ($data as $k=>$loc) {
                        $maxImageCountPerLoc += count($loc['images']);
                        $loc['urlParams'] = [
                            'sViewport'=>'detail',
                            'sArticle'=>$k
                        ];
                        if ($maxImageCountPerLoc < $limit) {
                            $data_array[$index][] = $loc;
                        } else {
                            $index++;
                            $maxImageCountPerLoc = 0;
                            $data_array[$index][] = $loc;
                            $maxImageCountPerLoc += count($loc['images']);
                        }
                    }
                } else {
                    $index = 0;
                    $i = 0;
                    foreacH($data as $k => $value){
                        if($i >= $limit) {
                            $index++;
                            $i = 0;
                        }
                        $data_array[$index][] = $value;
                        $i++;
                    }
                }
                $i = 0;
                foreach($data_array as $sitemapContent) {
                    $this->writeFile($key, $shop, $sitemapContent, $i);
                    $sitemaps[] = $key . '-' . $i;
                    $i++;
                }
            }
            $this->generateSitemapIndex($sitemaps,$shop);
        }
    }

    /**
     * @description returns true if area should be generated and false if not
     * @params $area string -> area to check
     */
    private function areaGeneratorRule ($area) {
        switch($area) {
            case "articles":
                return Shopware()->Config()->getByNamespace('PixupSitemapGenerator', 'useArticles');
            case "categories":
                return Shopware()->Config()->getByNamespace('PixupSitemapGenerator', 'useCategories');
            case "blogs":
                return Shopware()->Config()->getByNamespace('PixupSitemapGenerator', 'useBlogs');
            case "customPages":
                return Shopware()->Config()->getByNamespace('PixupSitemapGenerator', 'useCustomPages');
            case "suppliers":
                return Shopware()->Config()->getByNamespace('PixupSitemapGenerator', 'useSuppliers');
            case "landingPages":
                return Shopware()->Config()->getByNamespace('PixupSitemapGenerator', 'useLandingPages');
            case "pictures":
                return $usePictures = Shopware()->Config()->getByNamespace('PixupSitemapGenerator', 'usePictures');
            default:
                return false;
        }
    }

    private function generateSitemapIndex($sitemaps, $shop){
        $smarty = $this->container->get('template');
        $smarty->addTemplateDir(
            $this->getPath() . '/Resources/views/'
        );
        $sitemapRes = [];
        foreach($sitemaps as $k=>$name) {

            $sitemapRes[] = [
                'name' => $name,
                'url' => [
                    'sViewport' => 'PixupSitemap',
                    'sAction' => 'sitemap',
                    'area' => $name . '-sitemap.xml'
                ],
            ];
        }
        $filename = self::SITEMAP_FILE_CACHE_DIRE . 'index' . '_' . $shop['id'] . '.xml';
        $smarty->assign(['sitemaps' => $sitemapRes, 'lastmod' => new \DateTime('now'),'shopId' => $shop['id']]);
        $content = $smarty->fetch('frontend/pixup_cache_sitemap/index.tpl');
        file_put_contents($filename,$content);
    }
    private function writeFile($area, $shop, $data, $count){
        $sitemapGenerator = new SitemapGenerator($this->container);
        $shopInstance = new ShopDecorator();
        $shopInstance->setId($shop['id']);
        if(!empty($shop['main_id']) && $shop['main_id'] !== 0){
            $shopInstance->setMainId($shop['main_id']);
        }
        $useAlternate = Shopware()->Container()->get('shopware.plugin.config_reader')->
        getByPluginName('PixupSitemapGenerator',$shopInstance)['useAlternate'];
        $filename = self::SITEMAP_FILE_CACHE_DIRE . $area . '_' . $shop['id'] . '_' . $count . '.xml';
        /**
         * @var \Enlight_Template_Manager $smarty
         */
        $smarty = $this->container->get('template');
        $smarty->addTemplateDir($this->getPath() . '/Resources/views');
        $smarty->addPluginsDir($this->getPath() . '/Resources/_private/smarty');
        $smarty->assign([
            'data' => $data,
            'area' => $area,
            'languageShops'=>($useAlternate)?$sitemapGenerator->getLanguageShopsByShop(((empty($shop['main_id']))?$shop['id']:$shop['main_id']), $shop['id']):[],
            'config'=>[
                'useAlternate' =>$useAlternate
            ],
            'shopId' => $shop['id']
        ]);
        $content = $smarty->fetch('frontend/pixup_cache_sitemap/sitemap.tpl');
        file_put_contents($filename,$content);
    }
    public function generateCache() {
        $sql = 'DELETE FROM pixup_sitemap_cache';
        Shopware()->Db()->query($sql);
        $sql = "SELECT * FROM s_core_shops";

        $shops = Shopware()->Db()->fetchAll($sql);
        foreach ($shops as $shop) {

            foreach ($this->attributeTableMapping as $key => $area) {
                $data_array = array();
                $data = $this->getSitemapForArea($key, $shop);
                $now = new \DateTime('now');
                if ($key == "pictures") {
                    foreach ($data as $k => $loc) {
                        $images = json_encode($loc['images']);
                        $params = ['path'=>$loc['path'],'urlParams'=>['sViewport'=>'detail','sArticle'=>$k]];
                        $params = json_encode($params);
                        $sql = 'INSERT INTO pixup_sitemap_cache (type, created_at, shop_id, data_id, links, params)
                            VALUES (:type,  :createdAt, :shopId, :dataId, :links, :params);';
                        Shopware()->Db()->query($sql, [
                            'type' => $key,
                            'createdAt' => $now->format('Y-m-d H:i:s'),
                            'shopId' => $shop['id'],
                            'dataId' => $loc['id'],
                            'links' => $images,
                            'params' => $params
                        ]);
                    }
                } else {
                    foreach ($data as $loc) {
                        $params = json_encode($loc['urlParams']);
                        if ($loc['changed']) {
                            $loc['changed'] = $loc['changed']->format('Y-m-d H:i:s');
                        }
                        if ($loc['pixupProperties']['is_seo_landing']) {
                            $loc['pixupProperties']['is_seo_landing'] = $loc['pixupProperties']['is_seo_landing'];
                        } else {
                            $loc['pixupProperties']['is_seo_landing'] = false;
                        }
                        $sql = 'INSERT INTO pixup_sitemap_cache (type, created_at, shop_id, data_id, params, lastmode, frequency, priority, seo_langing_page)
                            VALUES (:type,  :createdAt, :shopId, :dataId, :params, :lastmod, :frequency, :priority, :isSeo);';

                        Shopware()->Db()->query($sql, [
                            'type' => $key,
                            'createdAt' => $now->format('Y-m-d H:i:s'),
                            'shopId' => $shop['id'],
                            'dataId' => $loc['id'],
                            'params' => $params,
                            'lastmod' => $loc['changed'],
                            'frequency' => $loc['pixupProperties']['changefreq'],
                            'priority' => $loc['pixupProperties']['priority'],
                            'isSeo' => $loc['pixupProperties']['is_seo_landing']
                        ]);
                    }
                }
            }
        }
    }

    /** @var array */
    private $attributeTableMapping = array(
        'categories' => array(
            'tableName' => 's_categories_attributes',
            'idField' => 'categoryID'
        ),
        'articles' => array(
            'tableName' => 's_articles_attributes',
            'idField' => 'articleDetailsID'
        ),
        'blogs' => array(
            'tableName' => 's_blog_attributes',
            'idField' => 'blog_id'
        ),
        'customPages' => array(
            'tableName' => 's_cms_static_attributes',
            'idField' => 'cmsStaticID'
        ),
        'suppliers' => array(
            'tableName' => 's_articles_supplier_attributes',
            'idField' => 'supplierID'
        ),
        'landingPages' => array(
            'tableName' => 's_emotion_attributes',
            'idField' => 'emotionID'
        ),
        'pictures' => []
    );

    private function getSitemapForArea($area, $shop) {
        $data = null;

        switch ($area) {
            case "articles":
                $data = $this->buildDefaultSitemap('articles', $shop);
                break;
            case "categories":
                $data = $this->buildDefaultSitemap('categories', $shop);
                $data = array_values($data);
                $data = $this->removeExcludedSubCategories($data);
                break;
            case "blogs":
                $data = $this->buildDefaultSitemap('blogs', $shop);
                break;
            case "customPages":
                $data = $this->buildDefaultSitemap('customPages', $shop);
                break;
            case "suppliers":
                $data = $this->buildDefaultSitemap('suppliers', $shop);
                break;
            case "landingPages":
                $data = $this->buildDefaultLandingPagesSitemap('landingPages', $shop);
                break;
            case "pictures":
                $data = $this->getPicturesCronSitemap($shop);
                break;
            default:
        }

        return $data;
    }

    /**
     * Build a sitemap for pictures
     *
     * @return array
     */
    private function getPicturesCronSitemap($shop) {
        $result = array();
        $mediaService = $this->container->get('shopware_media.media_service');
        $config = $this->container->get('config');
        // first, get all articles
        $sqlArticles = "SELECT sa.id as articleID,sa.main_detail_id as id FROM s_articles sa JOIN s_articles_attributes saa ON sa.main_detail_id=saa.articledetailsID WHERE sa.active = 1 AND saa.pixup_exclude_sitemap != 1";
        $articles = Shopware()->Db()->fetchAll($sqlArticles);

        $shopCategory = $shop['category_id'];

        if ($config->getByNamespace('PixupSitemapGenerator', 'dataBasedOn')==1) {
            $basedOn = 'articleID';
        } else {
            $basedOn = 'id';
        }

        foreach ($articles as $article) {
            $sqlCat = "SELECT * FROM s_articles_categories_ro sacr WHERE sacr.articleID = ? AND sacr.categoryID=?";
            $categories = Shopware()->Db()->fetchRow($sqlCat, array($article[$basedOn], $shopCategory));

            if ($categories) {

                $sqlMedia = "SELECT ai.media_id FROM s_articles_img ai WHERE ai.articleID = ?";

                $mediaIDs = Shopware()->Db()->fetchAll($sqlMedia, array($article[$basedOn]));

                if (count($mediaIDs) == 0) {
                    continue;
                } else {
                    // 3rd, get path for detail article
                    $detailPath = "sViewport=detail&sArticle=" . $article[$basedOn];

                    $sqlArticlePath = "SELECT path FROM s_core_rewrite_urls WHERE org_path = " . "\"" . $detailPath . "\" AND main = 1";
                    //$sqlArticlePath = "SELECT path FROM s_core_rewrite_urls WHERE org_path = $detailPath  AND main = 1";
                    $rewritePath = Shopware()->Db()->fetchRow($sqlArticlePath);
                    //$rewritePath['path'] =$mediaService->getUrl($rewritePath['path']);

                    if (is_null($rewritePath['path'])) {
                        $result[$article[$basedOn]]['path'] = "detail/index/sArticle/" . $article[$basedOn];
                    } else {
                        $result[$article[$basedOn]]['path'] = strtolower($rewritePath['path']);
                    }
                    foreach ($mediaIDs as $id) {
                        $sqlImages = "SELECT id, name, path FROM s_media WHERE type = 'IMAGE' AND id = ?";
                        $image = Shopware()->Db()->fetchRow($sqlImages, array($id['media_id']));
                        $url = $mediaService->getUrl($image['path']);
                        $result[$article[$basedOn]]['images'][] = $url;
                        $result[$article[$basedOn]]['id'] = $article[$basedOn];
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Remove all categories that are being removed via the "exclude subcategories" option
     *
     * @param $categoryEntries
     * @return mixed
     */
    private function removeExcludedSubCategories($categoryEntries) {
        $sql = "SELECT * FROM s_categories_attributes WHERE pixup_exclude_all_categories=1";

        $excludeCategories = Shopware()->Db()->fetchAll($sql);

        $countExcludedCategories = count($excludeCategories);
        $countAllCategories = count($categoryEntries);

        for ($i = 0; $i < $countAllCategories; $i++) {
            for ($j = 0; $j <= $countExcludedCategories; $j++) {
                $string = '|' . $excludeCategories[$j]['categoryID'] . '|';

                if (strpos($categoryEntries[$i]['path'], $string) > -1) {
                    unset($categoryEntries[$i]);
                }
            }
        }

        return $categoryEntries;
    }

    /**
     * Many sitemaps are created the same way. For those we have this general function. Used for instance for articles.
     *
     * @param $type
     * @return mixed
     */
    private function buildDefaultLandingPagesSitemap($type, $shop) {

        $content = $this->getSitemapContentCron($shop);

        $entries = $content[$type];

        $entries = array_values($entries);

        $countEntries = count($entries);
        for ($i = 0; $i < $countEntries; $i++) {
            $isUnset = false;
            if ($this->isExcludedFromSitemap($type, $entries[$i]['id'])) {
                unset($entries[$i]);
                $isUnset = true;
            }
            if (!$isUnset) {
                $entries[$i]['pixupProperties'] = $this->getEntryProperties($type, $entries[$i]['id']);
                $entries[$i]['pixupProperties']['is_seo_landing'] = false;
                if($type === 'landingPages'){
                    $id = $entries[$i]['urlParams']['emotionId'];
                    $entries[$i]['changed'] = $this->getLandingPagesCreateDate($id);
                }
            }
        }
        $existQuery = "SHOW TABLES LIKE 'tonur_seo_filter_landingpages'";
        $tableExists = Shopware()->Db()->fetchAll($existQuery);

        if (count($tableExists) > 0) {

            $shopId = $shop['id'];
            $sqlLandingPages = "SELECT * FROM tonur_seo_filter_landingpages tl WHERE tl.enabled=1 AND tl.shop_id=" . $shopId . ";";
            $landingPage = Shopware()->Db()->fetchAll($sqlLandingPages);

            foreach ($landingPage as $lp) {

                $pathArray = [];
                $entries[$countEntries] = $lp;
                $entries[$countEntries]['pixupProperties']['is_seo_landing'] = true;
                $entries[$countEntries]['pixupProperties']['changefreq'] = 'weekly';
                $entries[$countEntries]['pixupProperties']['priority'] = $lp['sitemap_prio'];

                $entries[$countEntries]['urlParams'] = Shopware()->Modules()->Core()->sRewriteLink($lp['canonical_url']) . $lp['canonical_url'];
                $countEntries++;
            }
        }

        return $entries;
    }

    /**
     * Many sitemaps are created the same way. For those we have this general function. Used for instance for articles.
     *
     * @param $type
     * @return mixed
     */
    private function buildDefaultSitemap($type, $shop) {

        $content = $this->getSitemapContentCron($shop);

        $entries = $content[$type];

        $entries = array_values($entries);

        $countEntries = count($entries);
        for ($i = 0; $i < $countEntries; $i++) {
            $isUnset = false;
            if ($this->isExcludedFromSitemap($type, $entries[$i]['id'])) {
                unset($entries[$i]);
                $isUnset = true;
            }
            if (!$isUnset) {
                // set lastmod for suppliers
                if(empty($entries[$i]['changed']) && Shopware()->Config()->getByNamespace('PixupSitemapGenerator', 'lastmod')) {
                    if($type === 'suppliers'){
                        $id = $entries[$i]['id'];
                        $entries[$i]['changed'] = $this->getSuppliersCreateDate($id);
                    }
                }
                $entries[$i]['pixupProperties'] = $this->getEntryProperties($type, $entries[$i]['id']);
            }
        }
        return $entries;
    }

    /**
     * Exclude categories from sitemap based on pixupSitemapMod_pixup_exclude_sitemap in s_categories_attributes
     *
     * @param $type
     * @param int $id object ID
     * @return bool true|false
     */
    private function isExcludedFromSitemap($type, $id) {
        if ($this->attributeTableMapping[$type]['tableName'] == 's_articles_attributes') {
            $sql = "SELECT pixup_exclude_sitemap FROM " . $this->attributeTableMapping[$type]['tableName'] .
                    " JOIN s_articles ON s_articles.main_detail_id=s_articles_attributes.articledetailsID WHERE " .
                    $this->attributeTableMapping[$type]['idField'] . " = ?";
        } else {
            $sql = "SELECT pixup_exclude_sitemap FROM " . $this->attributeTableMapping[$type]['tableName'] .
                    " WHERE " . $this->attributeTableMapping[$type]['idField'] . " = ?";
        }
        $exclude = Shopware()->Db()->fetchRow($sql, array($id));
        return $exclude['pixup_exclude_sitemap'];
    }

    /**
     * Get custom properties for url entry
     *
     * @param $type
     * @param $id
     * @return mixed
     */
    private function getEntryProperties($type, $id) {
        $sql = "SELECT IFNULL(pixup_link_link_change_frequenzy, 'weekly') AS changefreq, IFNULL(pixup_link_priority, '0.5') AS priority FROM " .
                $this->attributeTableMapping[$type]['tableName'] . " WHERE " . $this->attributeTableMapping[$type]['idField'] . " = ?";
        $pixupProperties = Shopware()->Db()->fetchRow($sql, array($id));
        return $pixupProperties;
    }

    /**
     * @return array
     */
    public function getSitemapContentCron($shop) {
        $parentId = $shop['category_id'];
        $categories = $this->readCategoryUrls($parentId);
        $categoryIds = array_column($categories, 'id');

        return [
            'categories' => $categories,
            'articles' => $this->readArticleUrls($categoryIds),
            'blogs' => $this->readBlogUrls($parentId),
            'customPages' => $this->readStaticUrls($shop),
            'suppliers' => $this->readSupplierUrls($shop),
            'landingPages' => $this->readLandingPageUrls($shop),
        ];
    }

    /**
     * Print category urls
     *
     * @param int $parentId
     *
     * @return array
     */
    private function readCategoryUrls($parentId) {
        $categoryRepository = Shopware()->Models()->getRepository('Shopware\Models\Category\Category');
        $categories = $categoryRepository->getActiveChildrenList($parentId);

        foreach ($categories as &$category) {
            $category['show'] = empty($category['external']);

            $category['urlParams'] = [
                'sViewport' => 'cat',
                'sCategory' => $category['id'],
                'title' => $category['name'],
            ];

            if ($category['blog']) {
                $category['urlParams']['sViewport'] = 'blog';
            }
        }

        return $categories;
    }

    /**
     * Read article urls
     *
     * @param int[] $categoryIds
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @return array
     */
    private function readArticleUrls(array $categoryIds) {
        if (empty($categoryIds)) {
            return [];
        }
        $sql = '
            SELECT DISTINCT sa.id, sa.changetime
            FROM s_articles sa
            LEFT JOIN s_articles_categories sac
            ON sac.articleID=sa.id
            WHERE sa.active = 1 AND sac.categoryID IN(?)
        ';

        $result = $this->container->get('dbal_connection')->executeQuery(
                $sql, [$categoryIds], [Connection::PARAM_INT_ARRAY]
        );

        $articles = [];
        while ($article = $result->fetch()) {

            $article['changed'] = new \DateTime($article['changetime']);
            $article['urlParams'] = [
                'sViewport' => 'detail',
                'sArticle' => $article['id'],
            ];

            $articles[] = $article;
        }

        return $articles;
    }

    /**
     * Reads the blog item urls
     *
     * @param int $parentId
     *
     * @return array
     */
    private function readBlogUrls($parentId) {
        $blogs = [];

        $categoryRepository = Shopware()->Models()->getRepository('Shopware\Models\Category\Category');
        $query = $categoryRepository->getBlogCategoriesByParentQuery($parentId);
        $blogCategories = $query->getArrayResult();

        $blogIds = [];
        foreach ($blogCategories as $blogCategory) {
            $blogIds[] = $blogCategory['id'];
        }
        if (empty($blogIds)) {
            return $blogs;
        }

        $sql = '
            SELECT id, category_id, DATE(display_date) as changed
            FROM s_blog
            WHERE active = 1 AND category_id IN(?)
        ';

        $result = $this->container->get('dbal_connection')->executeQuery(
                $sql, [$blogIds], [Connection::PARAM_INT_ARRAY]
        );

        while ($blog = $result->fetch()) {
            $blog['changed'] = new \DateTime($blog['changed']);
            $blog['urlParams'] = [
                'sViewport' => 'blog',
                'sAction' => 'detail',
                'sCategory' => $blog['category_id'],
                'blogArticle' => $blog['id'],
            ];

            $blogs[] = $blog;
        }

        return $blogs;
    }

    /**
     * Helper function to Read the static pages urls
     *
     * @return array
     */
    private function readStaticUrls($shop) {
        $shopId = $shop['id'];
        $sites = $this->getSitesByShopId($shopId);

        foreach ($sites as $site) {
            if (!empty($site['children'])) {
                $sites = array_merge($sites, $site['children']);
            }
        }

        foreach ($sites as &$site) {
            $site['urlParams'] = [
                'sViewport' => 'custom',
                'sCustom' => $site['id'],
            ];

            $site['show'] = $this->filterLink($site['link'], $site['urlParams']);
        }

        return $sites;
    }

    /**
     * Helper function to read all static pages of a shop from the database
     *
     * @param int $shopId
     *
     * @return array
     */
    private function getSitesByShopId($shopId) {
        $sql = '
            SELECT groups.key
            FROM s_core_shop_pages shopPages
              INNER JOIN s_cms_static_groups groups
                ON groups.id = shopPages.group_id
            WHERE shopPages.shop_id = ?
        ';

        $statement = $this->container->get('dbal_connection')->executeQuery($sql, [$shopId]);

        $keys = $statement->fetchAll(\PDO::FETCH_COLUMN);

        $siteRepository = Shopware()->Models()->getRepository('Shopware\Models\Site\Site');

        $sites = [];
        foreach ($keys as $key) {
            $current = $siteRepository->getSitesByNodeNameQueryBuilder($key, $shopId)
                    ->resetDQLPart('from')
                    ->from('Shopware\Models\Site\Site', 'sites', 'sites.id')
                    ->getQuery()
                    ->getArrayResult();

            $sites += $current;
        }

        return $sites;
    }

    /**
     * Helper function to filter predefined links, which should not be in the sitemap (external links, sitemap links itself)
     * Returns false, if the link is not allowed
     *
     * @param string $link
     * @param array  $userParams
     *
     * @return bool
     */
    private function filterLink($link, &$userParams) {
        if (empty($link)) {
            return true;
        }

        $userParams = parse_url($link, PHP_URL_QUERY);
        parse_str($userParams, $userParams);

        $blacklist = ['', 'sitemap', 'sitemapXml'];

        if (in_array($userParams['sViewport'], $blacklist)) {
            return false;
        }

        return true;
    }

    /**
     * Helper function to read the supplier pages urls
     *
     * @return array
     */
    private function readSupplierUrls($shop) {
        $suppliers = $this->getSupplierForSitemap($shop);
        foreach ($suppliers as &$supplier) {
            $supplier['urlParams'] = [
                'sViewport' => 'listing',
                'sAction' => 'manufacturer',
                'sSupplier' => $supplier['id'],
            ];
        }

        return $suppliers;
    }

    /**
     * Gets all suppliers that have products for the current shop
     *
     * @throws \Exception
     *
     * @return array
     */
    private function getSupplierForSitemap($shop) {

        $categoryId = $shop['category_id'];

        /** @var $query QueryBuilder */
        $query = $this->container->get('dbal_connection')->createQueryBuilder();
        $query->select(['manufacturer.id', 'manufacturer.name']);

        $query->from('s_articles_supplier', 'manufacturer');
        $query->innerJoin('manufacturer', 's_articles', 'product', 'product.supplierID = manufacturer.id')
                ->innerJoin('product', 's_articles_categories_ro', 'categories', 'categories.articleID = product.id AND categories.categoryID = :categoryId')
                ->setParameter(':categoryId', $categoryId);

        $query->groupBy('manufacturer.id');

        /** @var $statement \PDOStatement */
        $statement = $query->execute();

        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Helper function to read the landing pages urls
     *
     * @return array
     */
    private function readLandingPageUrls($shop) {
        $emotionRepository = Shopware()->Models()->getRepository('Shopware\Models\Emotion\Emotion');

        $shopId = $shop['id'];

        $builder = $emotionRepository->getCampaignsByShopId($shopId);
        $campaigns = $builder->getQuery()->getArrayResult();

        foreach ($campaigns as &$campaign) {
            $campaign['show'] = $this->filterCampaign($campaign['validFrom'], $campaign['validTo']);
            $campaign['urlParams'] = [
                'sViewport' => 'campaign',
                'emotionId' => $campaign['id'],
            ];
        }

        return $campaigns;
    }

    /**
     * Helper function to filter emotion campaigns
     * Returns false, if the campaign starts later or is outdated
     *
     * @param null $from
     * @param null $to
     *
     * @return bool
     */
    private function filterCampaign($from = null, $to = null) {
        $now = new \DateTime();

        if (isset($from) && $now < $from) {
            return false;
        }

        if (isset($to) && $now > $to) {
            return false;
        }

        return true;
    }

    protected function createSchema() {
        $tool = new SchemaTool($this->container->get('models'));
        $classes = [
            $this->container->get('models')->getClassMetadata(PixupSitemapCache::class)
        ];
        $tool->createSchema($classes);
    }

    protected function dropSchema() {
        $tool = new SchemaTool($this->container->get('models'));
        $classes = [
            $this->container->get('models')->getClassMetadata(PixupSitemapCache::class)
        ];
        $tool->dropSchema($classes);
    }

    private function getSuppliersCreateDate($id){
        $sql = "SELECT changed FROM s_articles_supplier WHERE id = ?";
        $res = Shopware()->Db()->fetchOne($sql, array($id));
        return (!empty($res)) ? new \DateTime($res) : null;
    }

    private function getLandingPagesCreateDate($id){
        $sql = "SELECT modified FROM s_emotion WHERE id = ?";
        $res = Shopware()->Db()->fetchOne($sql, array($id));
        return (!empty($res)) ? new \DateTime($res) : null;
    }
}
