<?php
namespace PixupSitemapGenerator\Components\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
/**
 * Class AddTemplatePluginDirCompilerPass
 */
class AddTemplatePluginDirCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $template = $container->getDefinition('template');
        $template->addMethodCall('addPluginsDir', [__DIR__ . '/../../Resources/_private/smarty']);
    }
}