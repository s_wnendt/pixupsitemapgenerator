<?php

/**
 * Pixup Sitemap Generator
 *
 * @author Michael Möhlihs
 */
class Shopware_Controllers_Frontend_PixupSitemap extends Enlight_Controller_Action {
    /** @var array */
    private $attributeTableMapping = array(
        'categories' => array(
            'tableName' => 's_categories_attributes',
            'idField' => 'categoryID'
        ),
        'articles' => array(
            'tableName' => 's_articles_attributes',
            'idField' => 'articleDetailsId'
        ),
        'blogs' => array(
            'tableName' => 's_blog_attributes',
            'idField' => 'blog_id'
        ),
        'customPages' => array(
            'tableName' => 's_cms_static_attributes',
            'idField' => 'cmsStaticID'
        ),
        'suppliers' => array(
            'tableName' => 's_articles_supplier_attributes',
            'idField' => 'supplierID'
        ),
        'landingPages' => array(
            'tableName' => 's_emotion_attributes',
            'idField' => 'emotionID'
        )
    );

    /**
     * Standard index action
     *
     * @param $type
     */
    public function indexAction($type) {

    }

    /**
     * Generate custom sitemap
     */
    public function sitemapAction() {
        $limit = Shopware()->Config()->getByNamespace('PixupSitemapGenerator', 'urlLimit');
        $cache = Shopware()->Config()->getByNamespace('PixupSitemapGenerator', 'useCache');
        $sitemapGenerator = new \PixupSitemapGenerator\Helper\SitemapGenerator($this->container);
        $area = $this->Request()->get('area');

        $number = $sitemapGenerator->extractNumberFromArea($area);

        $area = $sitemapGenerator->parseArea($area, $number);
        if ($cache == '1') {
            $data = $sitemapGenerator->getSitemapForAreaCache($area);
            $this->Response()->setHeader('Content-Type', 'text/xml; charset=utf-8');
            set_time_limit(0);
            $dataLength = count($data);
            if ($area == "pictures") {
                $data_array = array();
                $index = 0;
                $maxImageCountPerLoc = 0;
                foreach ($data as $k=>$loc) {
                    $loc['images'] = json_decode($loc['links']);
                    $maxImageCountPerLoc += count($loc['images']);
                    if ($maxImageCountPerLoc < $limit) {
                        $data_array[$index][] = $loc;
                    } else {
                        $index++;
                        $maxImageCountPerLoc = 0;
                        $data_array[$index][] = $loc;
                        $maxImageCountPerLoc += count($loc['images']);
                    }
                }

                $this->View()->assign(array(
                    'area' => $area,
                    'data' => $data_array[$number],
                ));
            } else {

                if ($dataLength > $limit) {
                    $length = $limit;
                    $data = array_slice($data, $number * $limit, $length, true);
                }

                $this->View()->assign(array(
                    'area' => $area,
                    'data' => $data,
                ));
            }
        }
        elseif ($cache == '2') {
            $shopId = Shopware()->Shop()->getId();
            $filename = \PixupSitemapGenerator\PixupSitemapGenerator::SITEMAP_FILE_CACHE_DIRE . $area . '_' . $shopId . '_' . $number . '.xml';
            $this->Response()->setHeader('Content-Type', 'text/xml; charset=utf-8');
            if(file_exists($filename)) {
                $content = file_get_contents($filename);
            } else {
                $content = "Please Execute the Cronjob in order to generate the Pixupmedia-Sitemap";
            }
            $this->View()->setTemplate(null);
            $this->Response()->setHeader('Content-Type', 'text/xml');
            return $this->Response()->setBody($content);
        } else {
            $data = $sitemapGenerator->getSitemapForArea($area);
            $this->Response()->setHeader('Content-Type', 'text/xml; charset=utf-8');
            set_time_limit(0);
            $dataLength = count($data);
            if ($area == "pictures") {
                $data_array = array();
                $index = 0;
                $maxImageCountPerLoc = 0;
                foreach ($data as $k=>$loc) {
                    $maxImageCountPerLoc += count($loc['images']);
                    $loc['urlParams'] = [
                        'sViewport'=>'detail',
                        'sArticle'=>$k
                    ];
                    if ($maxImageCountPerLoc < $limit) {
                        $data_array[$index][] = $loc;
                    } else {
                        $index++;
                        $maxImageCountPerLoc = 0;
                        $data_array[$index][] = $loc;
                        $maxImageCountPerLoc += count($loc['images']);
                    }
                }

                $this->View()->assign(array(
                    'area' => $area,
                    'data' => $data_array[$number],
                ));
            } else {

                if ($dataLength > $limit) {
                    $length = $limit;
                    $data = array_slice($data, $number * $limit, $length, true);
                }
                $this->View()->assign(array(
                    'area' => $area,
                    'data' => $data,
                ));
            }
        }

        //get all subshops
        $useAlternate = Shopware()->Container()->get('shopware.plugin.config_reader')->
            getByPluginName('PixupSitemapGenerator',Shopware()->Shop())['useAlternate'];
        $this->View()->assign(
            [
                'languageShops'=>($useAlternate)?$sitemapGenerator->getLanguageShopsByShop():[],
                'config'=>[
                    'useAlternate' =>$useAlternate
                ]
            ]
        );
    }
}
