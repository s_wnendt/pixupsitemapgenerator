<?php

namespace PixupSitemapGenerator\Models;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;


/**
 * Class PixupSitemapCache
 * @package PixupSitemapGenerator\Models
 * 
 * @ORM\Table(name="pixup_sitemap_cache")
 * @ORM\Entity
 */
class PixupSitemapCache extends ModelEntity{

    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $type;

    /**
     * @var int
     * @ORM\Column(name="data_id", type="integer")
     */
    protected $dataId;

    /**
     * @var int
     * @ORM\Column(name="shop_id", type="integer")
     */
    protected $shopId;

    /**
     * Keeps the meta description which is displayed in the HTML page.
     *
     * @var string
     *
     * @ORM\Column(name="params", type="text", nullable=true)
     */
    private $params;

    /**
     * Keeps the meta description which is displayed in the HTML page.
     *
     * @var string
     *
     * @ORM\Column(name="links", type="text", nullable=true)
     */
    private $links;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastmode;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $frequency;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $priority;

    /**
     * @var bool
     * @ORM\Column(name="seo_langing_page", type="boolean", nullable=false)
     */
    protected $isSeoLangingPage = false;

    /**
     * @var DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * @param string $shopId
     */
    public function setShopId($shopId) {
        $this->shopId = $shopId;
    }

    /**
     * @return string
     */
    public function getShopId() {
        return $this->shopId;
    }

    /**
     * @param string $dataId
     */
    public function setDataId($dataId) {
        $this->dataId = $dataId;
    }

    /**
     * @return string
     */
    public function getDataId() {
        return $this->dataId;
    }

    /**
     * @return string
     */
    public function getLastmode() {
        return $this->lastmode;
    }

    /**
     * @param string $lastmode
     */
    public function setLastmode($lastmode) {
        $this->lastmode = $lastmode;
    }

    /**
     * @return string
     */
    public function getFrequency() {
        return $this->frequency;
    }

    /**
     * @param string $frequency
     */
    public function setFrequency($frequency) {
        $this->frequency = $frequency;
    }

    /**
     * @return string
     */
    public function getPriority() {
        return $this->priority;
    }

    /**
     * @param string $priority
     */
    public function setPriority($priority) {
        $this->priority = $priority;
    }

    /**
     * @return string
     */
    public function getLinks() {
        return $this->links;
    }

    /**
     * @param string $links
     */
    public function setLinks($links) {
        $this->links = $links;
    }

    /**
     * @return string
     */
    public function getParams() {
        return $this->params;
    }

    /**
     * @param string $params
     */
    public function setParams($params) {
        $this->params = $params;
    }

    /**
     * @return string
     */
    public function getIsSeoLandingPage() {
        return $this->isSeoLangingPage;
    }

    /**
     * @param string $isSeoLangingPage
     */
    public function setIsSeoLandingPage($isSeoLangingPage) {
        $this->isSeoLangingPage = $isSeoLangingPage;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
        return $this;
    }

}
