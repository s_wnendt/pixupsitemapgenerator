<?php

namespace PixupSitemapGenerator\Models;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;
use Shopware\Models\Shop\Shop;


/**
 * Class PixupSitemapCache
 * @package PixupSitemapGenerator\Models
 *
 * @ORM\Table(name="pixup_sitemap_cache")
 * @ORM\Entity
 */
class ShopDecorator extends Shop {

    public function setId ($id) {
        $this->id = $id;
        return $this;
    }
    public function setMainId($mainId){
        $this->setMain((new ShopDecorator())->setId($mainId));
    }
}
