<?php
namespace PixupSitemapGenerator\Helper;
class SitemapGenerator {
    /** @var array */
    public $attributeTableMapping = array(
        'categories' => array(
            'tableName' => 's_categories_attributes',
            'idField' => 'categoryID'
        ),
        'articles' => array(
            'tableName' => 's_articles_attributes',
            'idField' => 'articleDetailsId'
        ),
        'blogs' => array(
            'tableName' => 's_blog_attributes',
            'idField' => 'blog_id'
        ),
        'customPages' => array(
            'tableName' => 's_cms_static_attributes',
            'idField' => 'cmsStaticID'
        ),
        'suppliers' => array(
            'tableName' => 's_articles_supplier_attributes',
            'idField' => 'supplierID'
        ),
        'landingPages' => array(
            'tableName' => 's_emotion_attributes',
            'idField' => 'emotionID'
        )
    );
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * @param null $shopId | main id of current Shop
     * @param null $currentShopId | shopId of current Shop
     * @return array|false
     */
    public function getLanguageShopsByShop($shopId=null, $currentShopId=null){
        if(empty($shopId) && empty($currentShopId)) {
            $context = Shopware()->Container()->get('shopware_storefront.context_service')->getShopContext();
            $shopId = $context->getShop()->getParentId();
            $currentShopId = $context->getShop()->getId();
        }
        $sqlCat = "SELECT cs.*,cl.locale as locale FROM s_core_shops cs 
                    INNER JOIN s_core_locales cl ON cs.locale_id = cl.id
                    WHERE cs.main_id = ? OR cs.id = ? OR cs.id = ?";
        $res = Shopware()->Db()->fetchAll($sqlCat, array($shopId,$currentShopId,$shopId));
        return $res;
    }

    /**
     * Generate the sitemap entries for a certain area
     *
     * "area" could be "articles", "categories" etc.
     *
     * @param $area
     * @return mixed|null
     */
    public function getSitemapForArea($area) {
        $data = null;

        switch ($area) {
            case "articles":
                $data = $this->buildDefaultSitemap('articles');
                break;
            case "categories":
                $data = $this->buildDefaultSitemap('categories');
                $data = array_values($data);
                $data = $this->removeExcludedSubCategories($data);
                break;
            case "blogs":
                $data = $this->buildDefaultSitemap('blogs');
                break;
            case "customPages":
                $data = $this->buildDefaultSitemap('customPages');
                break;
            case "suppliers":
                $data = $this->buildDefaultSitemap('suppliers');
                break;
            case "landingPages":
                $data = $this->buildDefaultLandingPagesSitemap('landingPages');
                break;
            case "pictures":
                $data = $this->getPicturesSitemap();
                break;
            default:
        }

        return $data;
    }

    public function getSitemapForAreaCache($area) {
        $data = null;

        switch ($area) {
            case "articles":
                $data = $this->buildDefaultSitemapCache('articles');
                break;
            case "categories":
                $data = $this->buildDefaultSitemapCache('categories');
                break;
            case "blogs":
                $data = $this->buildDefaultSitemapCache('blogs');
                break;
            case "customPages":
                $data = $this->buildDefaultSitemapCache('customPages');
                break;
            case "suppliers":
                $data = $this->buildDefaultSitemapCache('suppliers');
                break;
            case "landingPages":
                $data = $this->buildDefaultSitemapCache('landingPages');
                break;
            case "pictures":
                $data = $this->getPicturesSitemapCache();
                break;
            default:
        }

        return $data;
    }

    /**
     * Extract the number of the sitemap entries
     *
     * A sitemap has a certain limit as to how many entries it can carry. If that limit is passed a new list is generated.
     * Those lists are numbered starting from 0. That number is the one that is being extracted here. It is needed in
     * order to return the correct entries starting at a certain offset
     *
     * @param $area
     * @return int|mixed
     */
    public function extractNumberFromArea($area) {
        $number = filter_var($area, FILTER_SANITIZE_NUMBER_INT); // get number of area (if urls > 50.000)
        $number *= (-1); // there is a - before the number

        return $number;
    }

    /**
     * Parse the area into a usable string
     *
     * The "area" is the part of the sitemap that is going to be build. If the area is "articles", then only articles
     * will be considered for the sitemap.
     *
     * @param $area
     * @param $number
     *
     * @return int|mixed
     */
    public function parseArea($area, $number) {
        $area = str_replace('-sitemap.xml', '', $area); // remove '-sitemap.xml' from area
        $area = str_replace('-' . $number, '', $area); // remove number from area

        return $area;
    }

    /*     * ************************************************ Pictures **************************************************** */

    /**
     * Build a sitemap for pictures
     *
     * @return array
     */
    public function getPicturesSitemap() {
        $result = array();
        $mediaService = $this->container->get('shopware_media.media_service');
        $config = $this->container->get('config');
        if ($config->getByNamespace('PixupSitemapGenerator', 'dataBasedOn')==1) {
            $basedOn = 'articleID';
        } else {
            $basedOn = 'id';
        }

        // first, get all articles
        $sqlArticles = "SELECT * FROM s_articles sa JOIN s_articles_attributes saa ON sa.main_detail_id=saa.articledetailsID WHERE sa.active = 1 AND saa.pixup_exclude_sitemap != 1";
        $articles = Shopware()->Db()->fetchAll($sqlArticles);
        $context = Shopware()->Container()->get('shopware_storefront.context_service')->getShopContext();
        $shopCategory = $context->getShop()->getCategory()->getId();

        foreach ($articles as $article) {
            $sqlCat = "SELECT * FROM s_articles_categories_ro sacr WHERE sacr.articleID = ? AND sacr.categoryID=?";
            $categories = Shopware()->Db()->fetchRow($sqlCat, array($article[$basedOn], $shopCategory));

            if ($categories) {

                $sqlMedia = "SELECT ai.media_id FROM s_articles_img ai WHERE ai.articleID = ?";

                $mediaIDs = Shopware()->Db()->fetchAll($sqlMedia, array($article[$basedOn]));

                if (count($mediaIDs) == 0) {
                    continue;
                } else {
                    // 3rd, get path for detail article
                    $detailPath = "sViewport=detail&sArticle=" . $article[$basedOn];

                    $sqlArticlePath = "SELECT path FROM s_core_rewrite_urls WHERE org_path = " . "\"" . $detailPath . "\" AND main = 1";
                    //$sqlArticlePath = "SELECT path FROM s_core_rewrite_urls WHERE org_path = $detailPath  AND main = 1";
                    $rewritePath = Shopware()->Db()->fetchRow($sqlArticlePath);
                    //$rewritePath['path'] =$mediaService->getUrl($rewritePath['path']);

                    if (is_null($rewritePath['path'])) {
                        $result[$article[$basedOn]]['path'] = "detail/index/sArticle/" . $article[$basedOn];
                    } else {
                        $result[$article[$basedOn]]['path'] = strtolower($rewritePath['path']);
                    }
                    foreach ($mediaIDs as $id) {
                        $sqlImages = "SELECT id, name, path FROM s_media WHERE type = 'IMAGE' AND id = ?";
                        $image = Shopware()->Db()->fetchRow($sqlImages, array($id['media_id']));
                        $url = $mediaService->getUrl($image['path']);
                        $result[$article[$basedOn]]['images'][] = $url;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Build a sitemap for pictures
     *
     * @return array
     */
    public function getPicturesSitemapCache() {
        $context = Shopware()->Container()->get('shopware_storefront.context_service')->getShopContext();
        $shopId = $context->getShop()->getId();

        $sql = "SELECT * FROM pixup_sitemap_cache WHERE type = ? AND shop_id =?";
        $entries = Shopware()->Db()->fetchAll($sql, array('pictures', $shopId));


        $countEntries = count($entries);
        for ($i = 0; $i < $countEntries; $i++) {
            $entries[$i]['images'] = json_decode($entries[$i]['links']);
            $entries[$i]['urlParams'] = json_decode($entries[$i]['params'],true)['urlParams'];
            $entries[$i]['path'] = json_decode($entries[$i]['params'],true)['path'];
        }
        return $entries;
    }

    /*     * *********************************************** Helpers ****************************************************** */

    /**
     * Remove all categories that are being removed via the "exclude subcategories" option
     *
     * @param $categoryEntries
     * @return mixed
     */
    public function removeExcludedSubCategories($categoryEntries) {
        $sql = "SELECT * FROM s_categories_attributes WHERE pixup_exclude_all_categories=1";

        $excludeCategories = Shopware()->Db()->fetchAll($sql);

        $countExcludedCategories = count($excludeCategories);
        $countAllCategories = count($categoryEntries);

        for ($i = 0; $i < $countAllCategories; $i++) {
            for ($j = 0; $j <= $countExcludedCategories; $j++) {
                $string = '|' . $excludeCategories[$j]['categoryID'] . '|';

                if (strpos($categoryEntries[$i]['path'], $string) > -1) {
                    unset($categoryEntries[$i]);
                }
            }
        }

        return $categoryEntries;
    }

    /**
     * Many sitemaps are created the same way. For those we have this general function. Used for instance for articles.
     *
     * @param $type
     * @return mixed
     */
    public function buildDefaultLandingPagesSitemap($type) {
        $sitemap = $this->container->get('sitemapxml.repository');
        $content = $sitemap->getSitemapContent();

        $entries = $content[$type];
        $entries = array_values($entries);

        $countEntries = count($entries);
        for ($i = 0; $i < $countEntries; $i++) {
            $isUnset = false;
            if ($this->isExcludedFromSitemap($type, $entries[$i]['id'])) {
                unset($entries[$i]);
                $isUnset = true;
            }
            if (!$isUnset) {
                $entries[$i]['pixupProperties'] = $this->getEntryProperties($type, $entries[$i]['id']);
                $entries[$i]['pixupProperties']['is_seo_landing'] = false;
                if($type === 'landingPages'){
                    $id = $entries[$i]['urlParams']['emotionId'];
                    $entries[$i]['changed'] = $this->getLandingPagesCreateDate($id);
                }
            }
        }
        $existQuery = "SHOW TABLES LIKE 'tonur_seo_filter_landingpages'";
        $tableExists = Shopware()->Db()->fetchAll($existQuery);

        if (count($tableExists) > 0) {
            $context = Shopware()->Container()->get('shopware_storefront.context_service')->getShopContext();
            $shopId = $context->getShop()->getId();
            $sqlLandingPages = "SELECT * FROM tonur_seo_filter_landingpages tl WHERE tl.enabled=1 AND tl.shop_id=" . $shopId . ";";
            $landingPage = Shopware()->Db()->fetchAll($sqlLandingPages);

            foreach ($landingPage as $lp) {

                $pathArray = [];
                $entries[$countEntries] = $lp;
                $entries[$countEntries]['pixupProperties']['is_seo_landing'] = true;
                $entries[$countEntries]['pixupProperties']['changefreq'] = 'weekly';
                $entries[$countEntries]['pixupProperties']['priority'] = $lp['sitemap_prio'];

                $entries[$countEntries]['urlParams'] = Shopware()->Modules()->Core()->sRewriteLink($lp['canonical_url']) . $lp['canonical_url'];
                $countEntries++;
            }
        }

        return $entries;
    }

    /**
     * Many sitemaps are created the same way. For those we have this general function. Used for instance for articles.
     *
     * @param $type
     * @return mixed
     */
    public function buildDefaultSitemap($type) {
        $sitemap = $this->container->get('sitemapxml.repository');
        $content = $sitemap->getSitemapContent();
        $entries = $content[$type];
        $entries = array_values($entries);
        $countEntries = count($entries);
        for ($i = 0; $i < $countEntries; $i++) {
            $isUnset = false;
            if ($this->isExcludedFromSitemap($type, $entries[$i]['id'])) {
                unset($entries[$i]);
                $isUnset = true;
            }
            if (!$isUnset) {
                // set lastmod for suppliers
                if(empty($entries[$i]['changed']) && Shopware()->Config()->getByNamespace('PixupSitemapGenerator', 'lastmod')) {
                    if($type === 'suppliers'){
                        $id = $entries[$i]['id'];
                        $entries[$i]['changed'] = $this->getSuppliersCreateDate($id);
                    }
                }
                $entries[$i]['pixupProperties'] = $this->getEntryProperties($type, $entries[$i]['id']);
            }
        }
        return $entries;
    }

    /**
     * Many sitemaps are created the same way. For those we have this general function. Used for instance for articles.
     *
     * @param $type
     * @return mixed
     */
    public function buildDefaultSitemapCache($type) {
        $context = Shopware()->Container()->get('shopware_storefront.context_service')->getShopContext();
        $shopId = $context->getShop()->getId();
        $sql = "SELECT * FROM pixup_sitemap_cache WHERE type = ? AND shop_id =?";
        $entries = Shopware()->Db()->fetchAll($sql, array($type, $shopId));


        $countEntries = count($entries);
        for ($i = 0; $i < $countEntries; $i++) {
            $entries[$i]['urlParams'] = json_decode($entries[$i]['params']);
            $entries[$i]['changed'] = new \DateTime($entries[$i]['lastmode']);
            $entries[$i]['pixupProperties']['changefreq'] = $entries[$i]['frequency'];
            $entries[$i]['pixupProperties']['priority'] = $entries[$i]['priority'];
            $entries[$i]['pixupProperties']['is_seo_landing'] = $entries[$i]['seo_langing_page'];
        }

        return $entries;
    }

    /**
     * Exclude categories from sitemap based on pixupSitemapMod_pixup_exclude_sitemap in s_categories_attributes
     *
     * @param $type
     * @param int $id object ID
     * @return bool true|false
     */
    public function isExcludedFromSitemap($type, $id) {
        if ($this->attributeTableMapping[$type]['tableName'] == 's_articles_attributes') {
            $sql = "SELECT pixup_exclude_sitemap FROM " . $this->attributeTableMapping[$type]['tableName'] .
                " JOIN s_articles ON s_articles.main_detail_id=s_articles_attributes.articledetailsID WHERE " .
                's_articles.id' . " = ?";
        } else {
            $sql = "SELECT pixup_exclude_sitemap FROM " . $this->attributeTableMapping[$type]['tableName'] .
                " WHERE " . $this->attributeTableMapping[$type]['idField'] . " = ?";
        }
        $exclude = Shopware()->Db()->fetchRow($sql, array($id));
        return $exclude['pixup_exclude_sitemap'];
    }

    /**
     * Get custom properties for url entry
     *
     * @param $type
     * @param $id
     * @return mixed
     */
    public function getEntryProperties($type, $id) {
        $sql = "SELECT IFNULL(pixup_link_link_change_frequenzy, 'weekly') AS changefreq, IFNULL(pixup_link_priority, '0.5') AS priority FROM " .
            $this->attributeTableMapping[$type]['tableName'] . " WHERE " . $this->attributeTableMapping[$type]['idField'] . " = ?";
        $pixupProperties = Shopware()->Db()->fetchRow($sql, array($id));
        return $pixupProperties;
    }

    public function getSuppliersCreateDate($id){
        $sql = "SELECT changed FROM s_articles_supplier WHERE id = ?";
        $res = Shopware()->Db()->fetchOne($sql, array($id));
        return (!empty($res)) ? new \DateTime($res) : null;
    }

    public function getLandingPagesCreateDate($id){
        $sql = "SELECT modified FROM s_emotion WHERE id = ?";
        $res = Shopware()->Db()->fetchOne($sql, array($id));
        return (!empty($res)) ? new \DateTime($res) : null;
    }
}
